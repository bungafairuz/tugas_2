CREATE TABLE departemen (
    id int,
    nama varchar(255),
    primary key (id)
);

CREATE TABLE karyawan (
    id int,
    nama varchar(255),
    jenis_kelamin char,
    status varchar(255),
    tanggal_lahir date,
    tanggal_masuk date,
    departemen int,
    primary key (id),
    foreign key (departemen) references departemen(id)
);

insert into departemen
values  (1, 'Manajemen'),
        (2, 'Pengembangan Bisnis'),
        (3, 'Teknisi'),
        (4, 'Analis');

select * from departemen;

insert into karyawan
values  (1, 'Rizki Saputra', 'L', 'Menikah', date '1980-10-11', date '2011-1-1', 1),
        (2, 'Farhan Reza', 'L', 'Belum', date '1989-11-1', date '2011-1-1', 1),
        (3, 'Riyando Adi', 'L', 'Menikah', date '1977-1-25', date '2011-1-1', 1),
        (4, 'Diego Manuel', 'L', 'Menikah', date '1983-2-22', date '2012-9-4', 2),
        (5, 'Satya Laksana', 'L', 'Menikah', date '1981-1-12', date '2011-3-19', 2),
        (6, 'Miguel Hernandez', 'L', 'Menikah', date '1994-10-16', date '2014-6-15', 2),
        (7, 'Putri Persada', 'P', 'Menikah', date '1988-1-30', date '2013-4-14', 2),
        (8, 'Alma Safira', 'P', 'Menikah', date '1991-5-18', date '2013-9-28', 3),
        (9, 'Haqi Hafiz', 'L', 'Belum', date '1995-9-19', date '2015-3-9', 3),
        (10, 'Abi Isyawara', 'L', 'Belum', date '1991-6-3', date '2012-1-22', 3),
        (11, 'Maman Kresna', 'L', 'Belum', date '1993-8-21', date '2012-9-15', 3),
        (12, 'Nadia Aulia', 'P', 'Belum', date '1989-10-17', date '2012-5-7', 4),
        (13, 'Mutiara Rezki', 'P', 'Menikah', date '1988-3-23', date '2013-5-21', 4),
        (14, 'Dani Setiawan', 'L', 'Belum', date '1986-2-11', date '2014-11-30', 4),
        (15, 'Budi Putra', 'L', 'Belum', date '1995-10-23', date '2015-12-3', 4);

select * from karyawan;